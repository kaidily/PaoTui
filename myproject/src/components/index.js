import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/detail/:id',
    name: 'detail',
    components: {
      default: () => import('@/components/Detail')
    },
    props: {
      default: true
    }
  },
  {
    path: '/jinxing',
    name: 'jinxing',
    components: {
      default: () => import('@/components/Jinxing')
    }
  },
  {
    path: '/chongzhi',
    name: 'chongzhi',
    components: {
      default: () => import('@/components/Chongzhi')
    }
  },
  {
    path: '/invite',
    name: 'invite',
    components: {
      default: () => import('@/components/Invite')
    }
  },
  {
    path: '/manage',
    name: 'manage',
    components: {
      default: () => import('@/components/Manage')
    }
  },
  {
    path: '/local',
    name: 'local',
    components: {
      default: () => import('@/components/Local')
    }
  },
  {
    path: '/service',
    name: 'service',
    components: {
      default: () => import('@/components/Service')
    }
  },
  {
    path: '/message',
    name: 'message',
    components: {
      default: () => import('@/components/Message')
    }
  },
  {
    path: '/all',
    name: 'all',
    components: {
      default: () => import('@/components/All')
    }
  },
  {
    path: '/shouhuo',
    name: 'shouhuo',
    components: {
      default: () => import('@/components/Shouhuo')
    }
  },
  {
    path: '/jiedan',
    name: 'jiedan',
    components: {
      default: () => import('@/components/Jiedan')
    }
  },
  {
    path: '/zhifu',
    name: 'zhifu',
    components: {
      default: () => import('@/components/Zhifu')
    }
  },
  {
    path: '/balance',
    name: 'balance',
    components: {
      default: () => import('@/components/Balance')
    }
  },
  {
    path: '/persona',
    name: 'persona',
    components: {
      default: () => import('@/components/Persona')
    }
  },
  {
    path: '/home',
    name: 'home',
    components: {
      default: () => import('@/components/Home'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/kind',
    name: 'kind',
    components: {
      default: () => import('@/components/Kind'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/cart',
    name: 'cart',
    components: {
      default: () => import('@/components/Cart'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/userinfo',
    name: 'userinfo',
    components: {
      default: () => import('@/components/UserInfo')
    }
  },
  {
    path: '/user',
    name: 'user',
    components: {
      default: () => import('@/components/User'),
      footer: () => import('@/components/Footer')
    },
    children: [
      {
        path: '',
        redirect: 'noLogin'
      },
      {
        path: 'noLogin',
        component: () => import('@/components/UserNoLogin')
      },
      {
        path: 'login',
        component: () => import('@/components/UserLogin')
      }
    ]
  }
]

const router = new Router({
  routes: routes
})

export default router
