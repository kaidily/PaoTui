import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/detail/:id',
    name: 'detail',
    alias: '/h',
    components: {
      default: () => import('@/components/Detail')
    },
    props: {
      default: true
    }
  },
  {
    path: '/home',
    name: 'home',
    components: {
      default: () => import('@/components/Home'),
      footer: () => import('@/components/Footer')
    }
  },
  {
    path: '/buytime',
    name: 'buytime',
    components: {
      default: () => import('@/components/Buytime')
    }
  },
  {
    path: '/buy',
    name: 'buy',
    components: {
      default: () => import('@/components/Buy'),
      fukuan: () => import('@/components/Place')
    }
  },
  {
    path: '/dingdan',
    name: 'dingdan',
    components: {
      default: () => import('@/components/DingDan')
    }
  },
  {
    path: '/fankui',
    name: 'fankui',
    components: {
      default: () => import('@/components/fankui')
    }
  },
  {
    path: '/feiyong',
    name: 'feiyong',
    components: {
      default: () => import('@/components/Feiyong')
    }
  },
  {
    path: '/getpassword',
    name: 'getpassword',
    components: {
      default: () => import('@/components/GetPassword')
    }
  },
  {
    path: '/getphone',
    name: 'getphone',
    components: {
      default: () => import('@/components/GetPhone')
    }
  },
  {
    path: '/juan',
    name: 'juan',
    components: {
      default: () => import('@/components/Juan')
    }
  },
  {
    path: '/more',
    name: 'more',
    components: {
      default: () => import('@/components/More')
    }
  },
  {
    path: '/peisong',
    name: 'peisong',
    components: {
      default: () => import('@/components/Peisong')
    }
  },
  {
    path: '/set',
    name: 'set',
    components: {
      default: () => import('@/components/Set')
    }
  },
  {
    path: '/tousu',
    name: 'tousu',
    components: {
      default: () => import('@/components/Tousu')
    }
  },
  {
    path: '/pp',
    name: 'pp',
    components: {
      default: () => import('@/components/pp')
    }
  },
  {
    path: '/buylink',
    name: 'buylink',
    components: {
      default: () => import('@/components/Buylink')
    }
  },
  {
    path: '/buytake',
    name: 'buytake',
    components: {
      default: () => import('@/components/Buytake')
    }
  },
  {
    path: '/fetch',
    name: 'fetch',
    components: {
      default: () => import('@/components/Fetch'),
      footer: () => import('@/components/Footerr')
    }
  },
  {
    path: '/login',
    name: 'login',
    components: {
      default: () => import('@/components/Login')
    }
  },
  {
    path: '/register',
    name: 'register',
    components: {
      default: () => import('@/components/Register')
    }
  },
  {
    path: '/userinfo',
    name: 'userinfo',
    components: {
      default: () => import('@/components/UserInfo')
    }
  },
  {
    path: '/jinxing',
    name: 'jinxing',
    components: {
      default: () => import('@/components/Jinxing')
    }
  },
  {
    path: '/chongzhi',
    name: 'chongzhi',
    components: {
      default: () => import('@/components/Chongzhi')
    }
  },
  {
    path: '/invite',
    name: 'invite',
    components: {
      default: () => import('@/components/Invite')
    }
  },
  {
    path: '/manage',
    name: 'manage',
    components: {
      default: () => import('@/components/Manage')
    }
  },
  {
    path: '/local',
    name: 'local',
    components: {
      default: () => import('@/components/Local')
    }
  },
  {
    path: '/service',
    name: 'service',
    components: {
      default: () => import('@/components/Service')
    }
  },
  {
    path: '/message',
    name: 'message',
    components: {
      default: () => import('@/components/Message')
    }
  },
  {
    path: '/all',
    name: 'all',
    components: {
      default: () => import('@/components/All')
    }
  },
  {
    path: '/shouhuo',
    name: 'shouhuo',
    components: {
      default: () => import('@/components/Shouhuo')
    }
  },
  {
    path: '/jiedan',
    name: 'jiedan',
    components: {
      default: () => import('@/components/Jiedan')
    }
  },
  {
    path: '/zhifu',
    name: 'zhifu',
    components: {
      default: () => import('@/components/Zhifu')
    }
  },
  {
    path: '/balance',
    name: 'balance',
    components: {
      default: () => import('@/components/Balance')
    }
  },
  {
    path: '/persona',
    name: 'persona',
    components: {
      default: () => import('@/components/Persona')
    }
  },
  {
    path: '/forget',
    name: 'forget',
    components: {
      default: () => import('@/components/Forget')
    }
  },
  {
    path: '/city',
    name: 'city',
    components: {
      default: () => import('@/components/City')
    }
  },
  {
    path: '/give',
    name: 'give',
    components: {
      default: () => import('@/components/Give'),
      footer: () => import('@/components/Footerr')
    }
  },
  {
    path: '/help',
    name: 'help',
    components: {
      default: () => import('@/components/Help'),
      footer: () => import('@/components/Footerr')
    }
  },
  {
    path: '/lineup',
    name: 'lineup',
    components: {
      default: () => import('@/components/Lineup'),
      footer: () => import('@/components/Footerr')
    }
  },
  {
    path: '/place',
    name: 'place',
    components: {
      default: () => import('@/components/Place')
    }
  }
]

const router = new Router({
  routes: routes
})

export default router
